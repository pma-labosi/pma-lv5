package com.example.myapplication

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

private const val INTENT_EXTRA = "com.example.myapplication.INTENT_TRUE"

class CounterActivity : AppCompatActivity() {
    public lateinit var btnBack: Button
    public lateinit var tvAnswer: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_counter)

        btnBack = findViewById(R.id.btnBack)
        tvAnswer = findViewById(R.id.tvAnswer)

        val answerIsTrue = intent.getBooleanExtra(INTENT_EXTRA, false)
        tvAnswer.setText(answerIsTrue.toString())

        btnBack.setOnClickListener {
            finish()
        }
    }
    companion object {
        fun newIntent(packageContext: Context, answerIsTrue: Boolean): Intent {
            return Intent(packageContext, CounterActivity::class.java).apply {
                putExtra(INTENT_EXTRA, answerIsTrue)
            }
        }
    }
}