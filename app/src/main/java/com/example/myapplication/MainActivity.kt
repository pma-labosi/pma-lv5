package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.databinding.ActivityMainBinding

private const val TAG = "MainActivity";
private const val INTENT_EXTRA = "com.example.myapplication.INTENT_TRUE"

class MainActivity : AppCompatActivity() {
    public lateinit var binding: ActivityMainBinding
    public var currentIndex: Int = 0
    private val questionList = listOf(
        Question(R.string.question_android, true),
        Question(R.string.question_calculator, false),
        Question(R.string.question_unity, true),
        Question(R.string.question_json, true)
    )
    private val quizViewModel: QuizViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate(Bundle?) called")
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tvQuestion.setText(questionList[currentIndex].textResId)

        binding.btnTrue.setOnClickListener {
            checkAnswer(true);
        }
        binding.btnFalse.setOnClickListener {
            checkAnswer(false);
        }
        binding.btnNext.setOnClickListener {
            quizViewModel.moveToNext()
            updateQuestion()
        }
        binding.btnBack.setOnClickListener {
            quizViewModel.moveToPrevious()
            updateQuestion()
        }
        binding.btnNewActivity?.setOnClickListener {
            /*val answerIsTrue = quizViewModel.currentQuestionAnswer
                val intent = Intent(this, CounterActivity::class.java)
                intent.putExtra(INTENT_EXTRA, answerIsTrue)
                startActivity(intent)*/
            val answerIsTrue = quizViewModel.currentQuestionAnswer
            val intent = CounterActivity.newIntent(this@MainActivity, answerIsTrue)
            startActivity(intent)
        }
        updateQuestion()
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart() called")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume() called")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause() called")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop() called")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy() called")
    }

    private fun checkAnswer(userAnswer: Boolean){
        val correctAnswer = quizViewModel.currentQuestionAnswer
        val messageResId = if (userAnswer == correctAnswer) {
            R.string.correct_text
        } else {
            R.string.incorrect_text
        }
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT)
            .show()
    }

    private fun updateQuestion() {
        val questionTextResId = quizViewModel.currentQuestionText
        binding.tvQuestion.setText(questionTextResId)
    }

}